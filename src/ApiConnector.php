<?php
/**
 * Legal Text API Connector
 *
 * Copyright (C) 2020 Händlerbund Management AG <opensource@haendlerbund.de>
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Haendlerbund\LegalTextApiConnector;

abstract class ApiConnector
{
    const BASE_URI = 'https://legaltext-cache.haendlerbund.de/cache/';
    const API_KEY = '1IqJF0ap6GdDNF7HKzhFyciibdml8t4v';

    const HTTP_OK = 200;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_NOT_FOUND = 404;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_UNPROCESSABLE_ENTITY = 422;
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    private $baseUri;
    private $apiKey;
    private $curlTimeout = 20;
    private $invalidResponse = ['SHOP_NOT_FOUND', 'DOCUMENT_NOT_AVAILABLE', 'LANG_NOT_AVAILABLE'];

    /**
     * @param string $baseUri
     */
    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Set curl timeout in seconds
     *
     * @param $value
     *
     */
    public function setCurlTimeout($value)
    {
        $this->curlTimeout = $value;
    }

    /**
     * @return string
     */
    public function getBaseUri()
    {
        return $this->baseUri;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return int
     */
    public function getCurlTimeout()
    {
        return $this->curlTimeout;
    }

    /**
     * @param string $statusCode
     * @param string $messageKey
     * @param string $content
     * @return array
     */
    protected function buildResponse($statusCode, $messageKey, $content = '')
    {
        return compact("messageKey", "content", "statusCode");
    }

    /**
     * @param $queryString
     * @return array
     */
    protected function callApiWithCurl($queryString)
    {
        $url = $this->getBaseUri() . '?' . $queryString;
        $curl = curl_init();
        $options = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => $this->getCurlTimeout()
        ];
        curl_setopt_array($curl, $options);
        $content = curl_exec($curl);
        $info = curl_getinfo($curl);
        $statusCode = $info['http_code'];
        curl_close($curl);
        return $this->buildResponse($statusCode, '', $content);
    }

    /**
     * @param string $accessToken
     * @param array $parameters
     * @return array
     */
    protected function getApiResponse($accessToken, $parameters)
    {
        if (is_array($parameters) === false) {
            $content = 'Parameters must be an array';
            return $this->buildResponse(static::HTTP_UNPROCESSABLE_ENTITY, 'INVALID_PARAMETER', $content);
        }
        if ($this->isValidAccessTokenFormat($accessToken) === false) {
            $message = "Invalid Access Token format. Use the following format: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'";
            return $this->buildResponse(static::HTTP_UNAUTHORIZED, 'INVALID_ACCESS_TOKEN_FORMAT', $message);
        }
        $queryParams = array_intersect_key($parameters, array_flip(['lang', 'mode', 'did']));
        $queryParams['AccessToken'] = $accessToken;
        $queryParams['APIkey'] = $this->getApiKey();
        $queryString = http_build_query($queryParams);
        $retry = false;
        $attempts = 0;
        do {
            $response = $this->callApiWithCurl($queryString);
            if ($response['statusCode'] === static::HTTP_INTERNAL_SERVER_ERROR) {
                $attempts++;
                $retry = $attempts < 3;
            }
        } while ($retry);

        $mode = isset($queryParams['mode']) ? $queryParams['mode'] : '';
        if ($response['statusCode'] === static::HTTP_INTERNAL_SERVER_ERROR) {
            return $this->buildResponse($response['statusCode'], 'SERVICE_NOT_AVAILABLE');
        }
        if (empty($response['content']) && strtolower($mode) !== 'checktoken') {
            return $this->buildResponse(static::HTTP_NOT_FOUND, 'NO_CONTENT_RECEIVED');
        }
        if ($response['statusCode'] === static::HTTP_OK) {
            if(in_array($response['content'], $this->invalidResponse)) {
                return $this->buildResponse(static::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE');
            }
            return $this->buildResponse(static::HTTP_OK, '', $response['content']);
        }
        return $this->buildResponse($response['statusCode'], $response['content']);
    }

    /**
     * @param string $accessToken
     * @return bool
     */
    protected function isValidAccessTokenFormat($accessToken)
    {
        $pattern = '~^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$~';
        return (preg_match($pattern, $accessToken) ? true : false);
    }
}
