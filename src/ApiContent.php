<?php
/**
 * Legal Text API Connector
 *
 * Copyright (C) 2020 Händlerbund Management AG <opensource@haendlerbund.de>
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Haendlerbund\LegalTextApiConnector;

class ApiContent extends ApiConnector
{
    private $availableLegalTexts;
    private $documentApiModes = ['default', 'plain', 'text', 'classes', 'classes_head'];

    /**
     * ApiContent constructor.
     */
    public function __construct()
    {
        $this->availableLegalTexts = array();
        $this->setBaseUri(self::BASE_URI);
        $this->setApiKey(self::API_KEY);
    }

    /**
     * @param string $accessToken
     * @param string $legalTextId
     * @param string $mode
     * @param string $lang
     * @return array
     */
    public function getLegalText($accessToken, $legalTextId, $mode = 'default', $lang = 'de')
    {
        $parameters = [];
        $parameters['did'] = $legalTextId;
        if (empty($mode) || $this->isApiModeValidate($mode) === false) {
            $validModes = 'Valid modes are: ' . implode(', ', $this->documentApiModes);
            $message = empty($mode) ? ('Mode is required.') : ($mode . ' is invalid.');
            $message .= ' ' . $validModes;
            return $this->buildResponse(static::HTTP_UNPROCESSABLE_ENTITY, 'INVALID_MODE', $message);
        }
        $parameters['mode'] = $mode;
        if (isset($lang) && strtolower($lang) !== 'de') {
            $parameters['lang'] = $lang;
        }
        return $this->getApiResponse($accessToken, $parameters);
    }

    /**
     * @param string $accessToken
     * @param bool $force
     * @param string $lang
     * @return array
     */
    public function getAvailableLegalTexts($accessToken, $lang = 'de', $force = false)
    {
        if (empty($this->availableLegalTexts) || $force) {
            $parameters = ['mode' => 'documents', 'lang' => $lang];
            $response = $this->getApiResponse($accessToken, $parameters);
            if ($response['statusCode'] !== static::HTTP_OK) {
                return $response;
            }
            $responseDecode = json_decode($response['content']);
            if (empty($responseDecode)) {
                return $this->buildResponse(static::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE');
            }
            $this->availableLegalTexts = $responseDecode;
        }
        return $this->buildResponse(static::HTTP_OK, '', $this->availableLegalTexts);
    }

    /**
     * @param string $accessToken
     * @param string | null $documentId
     * @return array
     */
    public function getAvailableLanguages($accessToken, $documentId = null)
    {
        $parameters = ['mode' => 'trans'];
        if ($documentId !== null) {
            $parameters['did'] = $documentId;
        } else {
            $response = $this->getAvailableLegalTexts($accessToken);
            if ($response['statusCode'] !== static::HTTP_OK) {
                return $response;
            }
            $documentIds = array_keys((array)$response['content']);
            $parameters['did'] = current($documentIds);
        }
        $response = $this->getApiResponse($accessToken, $parameters);
        if ($response['statusCode'] !== static::HTTP_OK) {
            return $response;
        }
        $responseDecode = json_decode($response['content']);
        if (empty($responseDecode)) {
            return $this->buildResponse(static::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE');
        }
        array_unshift($responseDecode, 'de');
        $responseDecode = array_unique($responseDecode);
        return $this->buildResponse($response['statusCode'], $response['messageKey'], $responseDecode);
    }

    /**
     * @param string $accessToken
     * @param string $mode
     * @param string $lang
     * @return array
     */
    public function getLegalTexts($accessToken, $mode = 'default', $lang = 'de')
    {
        $availableLegalTexts = $this->getAvailableLegalTexts($accessToken, $lang);
        if ($availableLegalTexts['statusCode'] !== static::HTTP_OK) {
            return $availableLegalTexts;
        }
        $documents = [];
        foreach ($availableLegalTexts['content'] as $legalTextId => $name) {
            $document = new Document();
            $response = $this->getLegalText($accessToken, $legalTextId, $mode, $lang);
            if ($response['statusCode'] === static::HTTP_UNPROCESSABLE_ENTITY) {
                return $response;
            }
            if ($response['messageKey'] === 'SHOP_NOT_FOUND' || $response['messageKey'] === 'LANG_NOT_AVAILABLE') {
                return $this->buildResponse(static::HTTP_NOT_FOUND, $response['messageKey']);
            }
            if ($response['statusCode'] !== static::HTTP_OK) {
                continue;
            }
            $document->id = $legalTextId;
            $document->name = $name;
            $document->text = $response['content'];
            $documents[$legalTextId] = $document;
        }
        return $this->buildResponse(static::HTTP_OK, '', $documents);
    }

    /**
     * @param string $accessToken
     * @return array
     */
    public function validateAccessToken($accessToken)
    {
        if ($this->isValidAccessTokenFormat($accessToken)) {
            $response = $this->getApiResponse($accessToken, ['mode' => 'checktoken']);
            if ($response['statusCode'] === static::HTTP_OK) {
                return $this->buildResponse(static::HTTP_OK, 'VALID_ACCESS_TOKEN');
            }
            return $this->buildResponse(static::HTTP_UNAUTHORIZED, 'INVALID_ACCESS_TOKEN');
        }
        return $this->buildResponse(static::HTTP_UNAUTHORIZED, 'INVALID_ACCESS_TOKEN_FORMAT');
    }

    /**
     *
     * @param string $mode
     * @return string
     */
    private function isApiModeValidate($mode)
    {
        if (in_array($mode, $this->documentApiModes)) {
            return true;
        }
        return false;
    }
}