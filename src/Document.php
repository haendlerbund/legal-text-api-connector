<?php
/**
 * Legal Text API Connector
 *
 * Copyright (C) 2020 Händlerbund Management AG <opensource@haendlerbund.de>
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Haendlerbund\LegalTextApiConnector;

class Document
{
    public $id;
    public $name;
    public $text;
}