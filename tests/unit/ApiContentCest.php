<?php

use Haendlerbund\LegalTextApiConnector\ApiContent;
use Haendlerbund\LegalTextApiConnector\Document;
use Codeception\Example;

class ApiContentCest
{
    /** @var \Haendlerbund\LegalTextApiConnector\ApiContent $apiContent */
    private $apiContent = null;

    private $meteosatUrl = null;

    private $accessToken = '00000000-0000-0000-0000-000000000000';

    private $accessToken2 = '00000000-0000-0000-0000-000000000001';

    private $invalidAccessToken = '00000000-0000-0000-0000-000000000111';

    private $languages = [
        'de',
        'fr',
        'en',
        'pl',
        'es',
        'it',
        'nl',
        'ar'
    ];

    public function _before()
    {
        if ($this->meteosatUrl === null) {
            $this->meteosatUrl = 'http://localhost:9001/www/hbm/api/live_rechtstexte.htm';
            $meteosatUrlFromEnv = getenv('METEOSAT_URL');
            if (empty($meteosatUrlFromEnv) === false) {
                $this->meteosatUrl = $meteosatUrlFromEnv;
            }
        }
        $this->apiContent = new ApiContent();
        $this->apiContent->setBaseUri($this->meteosatUrl);
    }

    public function _after()
    {
        Mockery::close();
    }

    /**
     * @param \UnitTester $I
     */
    public function setBaseUri(UnitTester $I)
    {

        $this->apiContent = new ApiContent();
        $I->assertEquals(ApiContent::BASE_URI, $this->apiContent->getBaseUri());
        $newUri = "http://example.com";
        $this->apiContent->setBaseUri($newUri);
        $I->assertEquals($newUri, $this->apiContent->getBaseUri());
    }

    /**
     * @param \UnitTester $I
     */
    public function setApiKey(UnitTester $I)
    {
        $I->assertEquals(ApiContent::API_KEY, $this->apiContent->getApiKey());
        $newApiKey = 'gT2eSCsFsdXjkfEwelr0k92COSQoBLkpIUIcvPveZ1dJFJ8gAqEaMUonQiwtWdtf';
        $this->apiContent->setApiKey($newApiKey);
        $I->assertEquals($newApiKey, $this->apiContent->getApiKey());
    }

    /**
     * @param \UnitTester $I
     */
    public function setCurlTimeout(UnitTester $I)
    {
        $I->assertEquals(20, $this->apiContent->getCurlTimeout());
        $newTimeout = 60;
        $this->apiContent->setCurlTimeout($newTimeout);
        $I->assertEquals($newTimeout, $this->apiContent->getCurlTimeout());
    }

    /**
     * @param \UnitTester $I
     */
    public function documentModeIsInvalidResponse(UnitTester $I)
    {
        $message = 'error is invalid. Valid modes are: default, plain, text, classes, classes_head';
        $expectedResult = $this->buildResponse(ApiContent::HTTP_UNPROCESSABLE_ENTITY, 'INVALID_MODE', $message);
        $result = $this->apiContent->getLegalTexts($this->accessToken2, 'error');
        $I->assertEquals($expectedResult, $result);

        $message = 'Mode is required. Valid modes are: default, plain, text, classes, classes_head';
        $expectedResult = $this->buildResponse(ApiContent::HTTP_UNPROCESSABLE_ENTITY, 'INVALID_MODE', $message);
        $result = $this->apiContent->getLegalTexts('ed25f396-309b-11ea-978f-2e728ce88125', '');
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $lang
     *
     * @dataProvider lang
     */
    public function getAvailableLegalTexts(UnitTester $I, Example $lang)
    {
        $availableLegalTexts = $this->apiContent->getAvailableLegalTexts($this->accessToken, $lang[0]);
        $expected = [
            'messageKey' => "",
            'content' => (object)$this->availableLegalTexts($lang[0], 'name'),
            'statusCode' => ApiContent::HTTP_OK
        ];
        $I->assertEquals($expected, $availableLegalTexts);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $availableLanguages
     *
     * @dataProvider availableLanguages
     */
    public function getAvailableLanguages(UnitTester $I, Example $availableLanguages)
    {
        $accessToken = $availableLanguages['accessToken'];
        $documentId = $availableLanguages['documentId'];
        $expected = $availableLanguages['expected'];
        $result = $this->apiContent->getAvailableLanguages($accessToken, $documentId);
        $I->assertEquals($expected, $result);
    }

    public function getAvailableLanguagesInvalidResponse(UnitTester $I)
    {
        $apiContent = Mockery::mock(ApiContent::class . '[callApiWithCurl]')->shouldAllowMockingProtectedMethods();
        $response = $this->buildResponse(ApiContent::HTTP_OK, '', 'invalid json');
        $apiContent->shouldReceive('callApiWithCurl')->andReturn($response);
        /** @var \Haendlerbund\LegalTextApiConnector\ApiContent $apiContent */
        $expected = $this->buildResponse(ApiContent::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE');
        $result = $apiContent->getAvailableLanguages($this->accessToken, '123456');
        $I->assertEquals($expected, $result);

    }

    /**
     * @param \UnitTester $I
     */
    public function getAvailableLegalTextsInvalidResponse(UnitTester $I)
    {
        $expectedResult = $this->buildResponse(ApiContent::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE');
        $result = $this->apiContent->getAvailableLegalTexts($this->invalidAccessToken);
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param UnitTester $I
     * @param Example $tokens
     *
     * @dataProvider tokens
     */
    public function accessTokenFormatIsInvalidResponse(UnitTester $I, Example $tokens)
    {
        $message = "Invalid Access Token format. Use the following format: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'";
        $accessToken = $tokens['accessToken'];
        $expectedResult = $this->buildResponse(ApiContent::HTTP_UNAUTHORIZED, 'INVALID_ACCESS_TOKEN_FORMAT', $message);
        $result = $this->apiContent->getLegalTexts($accessToken, '');
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $data
     *
     * @dataProvider data
     */
    public function validateAccessToken(UnitTester $I, Example $data)
    {
        $accessToken = $data[0]['accessToken'];
        $expectedMessageKey = $data[0]['expectedMessageKey'];

        $result = $this->apiContent->validateAccessToken($accessToken);
        if ($expectedMessageKey === 'VALID_ACCESS_TOKEN') {
            $expected = $this->buildResponse(ApiContent::HTTP_OK, $expectedMessageKey);
        } else {
            $expected = $this->buildResponse(ApiContent::HTTP_UNAUTHORIZED, $expectedMessageKey);
        }
        $I->assertEquals($expected, $result);
    }

    /**
     * @param \UnitTester $I
     */
    public function getAvailableLegalTextsInternalServerErrorTryThreeTimes(UnitTester $I)
    {
        $expectedResult = $this->buildResponse(ApiContent::HTTP_INTERNAL_SERVER_ERROR, 'SERVICE_NOT_AVAILABLE');
        $result = $this->apiContent->getAvailableLegalTexts('10000000-0000-0000-0000-000000000001');
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param \UnitTester $I
     */
    public function getAvailableLegalTextsTimeout(UnitTester $I)
    {
        $expectedResult = $this->buildResponse(ApiContent::HTTP_NOT_FOUND, 'NO_CONTENT_RECEIVED');
        $result = $this->apiContent->getAvailableLegalTexts('00000000-0000-0000-0000-000000000501');
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $lang
     *
     * @dataProvider lang
     */
    public function getLegalText(UnitTester $I, Example $lang)
    {
        $documentId = '12766C46A8A';
        $availableLegalTexts = $this->availableLegalTexts($lang[0], 'text');
        $expectedResult = $this->buildResponse(ApiContent::HTTP_OK, '', $availableLegalTexts[$documentId]);
        $result = $this->apiContent->getLegalText($this->accessToken, $documentId, 'default',
            $lang[0]);
        $I->assertEquals($expectedResult, $result);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $lang
     *
     * @dataProvider lang
     */
    public function getLegalTexts(UnitTester $I, Example $lang)
    {
        $documents = $this->apiContent->getLegalTexts($this->accessToken, 'default', $lang[0]);
        $I->assertEquals($this->getExpectedDocuments([ApiContent::HTTP_OK], $lang[0]), $documents['content']);

        $documentsWithoutBattery = $this->apiContent->getLegalTexts($this->accessToken2, 'default', $lang[0]);
        $responseCodes = array_fill(0, 7, ApiContent::HTTP_OK);
        $responseCodes[] = ApiContent::HTTP_NOT_FOUND;
        $I->assertEquals($this->getExpectedDocuments($responseCodes, $lang[0]), $documentsWithoutBattery['content']);
    }

    /**
     * @param \UnitTester $I
     * @param \Codeception\Example $response
     *
     * @dataProvider responses
     */
    public function getLegalTextInvalidResponse(UnitTester $I, Example $response)
    {
        $apiContent = Mockery::mock(ApiContent::class . '[callApiWithCurl, getAvailableLegalTexts]')->shouldAllowMockingProtectedMethods();
        $apiContent->shouldReceive('callApiWithCurl')->andReturn($response['content']);
        $responseClosure = $this->getResponseCallback(0, 'de');
        $availableLegalTexts = $responseClosure();
        $availableLegalTexts['content'] = json_decode($availableLegalTexts['content']);
        $apiContent->shouldReceive('getAvailableLegalTexts')->andReturn($availableLegalTexts);
        /** @var \Haendlerbund\LegalTextApiConnector\ApiContent $apiContent */
        $result = $apiContent->getLegalTexts('ed25f396-309b-11ea-978f-2e728ce88125');
        $I->assertEquals($result, $response['expected']);
    }

    /**
     * @return array
     */
    private function lang()
    {
        return [
            ["de"],
            ["en"]
        ];
    }

    private function availableLanguages()
    {
        return [
            [
                'accessToken' => $this->accessToken,
                'documentId' => null,
                'expected' => $this->buildResponse(ApiContent::HTTP_OK, '', $this->languages)
            ],
            [
                'accessToken' => $this->accessToken,
                'documentId' => '12766C46A8A',
                'expected' => $this->buildResponse(ApiContent::HTTP_OK, '', $this->languages)
            ],
            [
                'accessToken' => $this->invalidAccessToken,
                'documentId' => null,
                'expected' => $this->buildResponse(ApiContent::HTTP_NOT_ACCEPTABLE, 'INVALID_RESPONSE', '')
            ],
            [
                'accessToken' => $this->invalidAccessToken,
                'documentId' => '123456',
                'expected' => $this->buildResponse(ApiContent::HTTP_BAD_REQUEST, 'SHOP_NOT_FOUND', '')
            ],
        ];
    }

    /**
     * @return array
     */
    private function data()
    {
        return [
            [
                [
                    'accessToken' => 'ed-5f396-309b-11ea-978f-2e728ce88125',
                    'expectedMessageKey' => 'INVALID_ACCESS_TOKEN_FORMAT'
                ]
            ],
            [
                [
                    'accessToken' => 'ed-5f396-309b-11ea-978f-2e728ce88125',
                    'expectedMessageKey' => 'INVALID_ACCESS_TOKEN_FORMAT'
                ]
            ],
            [
                [
                    'accessToken' => $this->accessToken,
                    'expectedMessageKey' => 'VALID_ACCESS_TOKEN'
                ]
            ],
            [
                [
                    'accessToken' => 'ed25f396-309b-11ea-978f-2e728ce88125',
                    'expectedMessageKey' => 'INVALID_ACCESS_TOKEN'
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    private function tokens()
    {
        return [
            ['accessToken' => 'ed-5f396-309b-11ea-978f-2e728ce88125'],
            ['accessToken' => 'ed25f396--309b-11ea-978f-2e728ce88125'],
            ['accessToken' => ' ed25f396-309b-11ea-978f-2e728ce88125'],
            ['accessToken' => 'ed25f396-309b-11ea-978f-2e728ce88125 '],
            ['accessToken' => 'ed25f396 - 309b - 11ea - 978f - 2e728ce88125'],
            ['accessToken' => '-ed-5f396-309b-11ea-978f-2e728c--8125'],
            ['accessToken' => 'ed25f396+309b+11ea+978f+2e728ce88125'],
            ['accessToken' => 'ed25f396-309b-11ea-978f-2e728+e88125'],
            ['accessToken' => 'ed25f396-309b-11ea-978f-2e728ce881256'],
            ['accessToken' => 'ed25f.96-30.b-11ea-978f-2e728ce88125'],
            ['accessToken' => 'ed25f396309b11ea978f2e728ce88125'],
            ['accessToken' => 'ed25f396-309b-11ea-978f'],
            ['accessToken' => ''],
        ];
    }

    /**
     * @return array
     */
    private function responses()
    {
        return [
            [
                'content' => $this->buildResponse(ApiContent::HTTP_BAD_REQUEST, '', 'SHOP_NOT_FOUND'),
                'expected' => $this->buildResponse(ApiContent::HTTP_NOT_FOUND, 'SHOP_NOT_FOUND')
            ],
            [
                'content' => $this->buildResponse(ApiContent::HTTP_BAD_REQUEST, '', 'DOCUMENT_NOT_AVAILABLE'),
                'expected' => $this->buildResponse(ApiContent::HTTP_OK, '', [])
            ],
            [
                'content' => $this->buildResponse(ApiContent::HTTP_OK, '', 'DOCUMENT_NOT_AVAILABLE'),
                'expected' => $this->buildResponse(ApiContent::HTTP_OK, '', [])
            ],
            [
                'content' => $this->buildResponse(ApiContent::HTTP_BAD_REQUEST, '', 'LANG_NOT_AVAILABLE'),
                'expected' => $this->buildResponse(ApiContent::HTTP_NOT_FOUND, 'LANG_NOT_AVAILABLE')
            ],
            [
                'content' => $this->buildResponse(ApiContent::HTTP_OK, ''),
                'expected' => $this->buildResponse(ApiContent::HTTP_OK, '', [])
            ]
        ];
    }

    /**
     * @param string $lang
     * @param string $response
     *
     * @return array
     */

    private function availableLegalTexts($lang, $response)
    {
        $result = [];
        $availableLegalTexts = [
            '12766C46A8A' => [
                'de' => [
                    'name' => 'Allgemeine Geschäftsbedingungen',
                    'text' => '<span style="font-size:18px;"><strong>Allgemeine Gesch&auml;ftsbedingungen und Kundeninformationen</strong></span><br />'
                ],
                'en' => [
                    'name' => 'Terms and Conditions',
                    'text' => '<span style="font-size:18px;"><strong>Standard Business Terms and customer information</strong></span><br />'
                ]
            ],
            '12766C53647' => [
                'de' => [
                    'name' => 'Widerrufsrecht',
                    'text' => '<span style="font-size:14px;"><strong><span style="font-size:18px;">Widerrufsrecht&nbsp;</span></strong></span><br />'
                ],
                'en' => [
                    'name' => 'Right of withdrawal',
                    'text' => '<span style="font-size:18px;"><strong>Right of withdrawal</strong></span><br />'
                ]
            ],
            '1452C24576D' => [
                'de' => [
                    'name' => 'Widerrufsrecht für digitale Waren',
                    'text' => '<span style="font-size:14px;"><span style="font-size:18px;"><strong>Widerrufsrecht f&uuml;r digitale Waren</strong></span><br />'
                ],
                'en' => [
                    'name' => 'Right of withdrawal for digital goods',
                    'text' => '<span style="font-size:18px;"><strong>Right of withdrawal for digital goods</strong></span><br />'
                ]
            ],
            '1463C5DBF05' => [
                'de' => [
                    'name' => 'Widerrufsrecht für Dienstleistungen',
                    'text' => '<span style="font-size:14px;"><span style="font-size:18px;"><strong>Widerrufsrecht f&uuml;r Dienstleistungen</strong></span><br />'
                ],
                'en' => [
                    'name' => 'Right of withdrawal for services',
                    'text' => '<span style="font-size:18px;"><strong>Right of withdrawal for services</strong></span><br />'
                ]
            ],
            '12766C58F26' => [
                'de' => [
                    'name' => 'Zahlung und Versand',
                    'text' => '<span style="font-size:18px;"><strong>Zahlung und Versand</strong></span><br /><br />'
                ],
                'en' => [
                    'name' => 'Payment and dispatch',
                    'text' => '<span style="font-size:20px;"><strong>Payment and dispatch</strong></span><br />'
                ]
            ],
            '160DEDA9674' => [
                'de' => [
                    'name' => 'Datenschutzerklärung',
                    'text' => '<span style="font-size:22px;"><strong>Datenschutzerkl&auml;rung</strong></span><br />'
                ],
                'en' => [
                    'name' => 'Data protection declaration',
                    'text' => '<span style="font-size:22px;"><strong>Data protection declaration</strong></span><br />'
                ]
            ],
            '1293C20B491' => [
                'de' => [
                    'name' => 'Impressum',
                    'text' => '<span style="font-size: 14px;"><strong><span style="font-size:16px;">Impressum</span></strong></span><br />'
                ],
                'en' => [
                    'name' => 'imprint',
                    'text' => '<strong><span style="font-size:16px;">About Us</span></strong><br />'
                ]
            ],
            '134CBB4D101' => [
                'de' => [
                    'name' => 'Batteriehinweise',
                    'text' => '<span style="font-size:14px;"><strong>Hinweise zur Batterieentsorgung</strong><br />'
                ],
                'en' => [
                    'name' => 'Information for battery disposal',
                    'text' => '<span style="font-size:14px;"><strong>Information for battery disposal</strong><br />'
                ]
            ]
        ];
        $firstLegalText = current($availableLegalTexts);
        if (array_key_exists($lang, $firstLegalText) === false) {
            $lang = 'de';
        }
        if (array_key_exists($response, $firstLegalText[$lang]) === false) {
            $response = 'name';
        }

        foreach ($availableLegalTexts as $id => $text) {
            $result[$id] = $text[$lang][$response];
        }
        return $result;
    }

    /**
     * @param int|null $index
     * @param string $lang
     *
     * @return \Closure
     */
    private function getResponseCallback($index, $lang)
    {
        $availableLegalTexts = $this->availableLegalTexts($lang, 'name');
        $legalTexts = $this->availableLegalTexts($lang, 'text');
        $contents = [json_encode($availableLegalTexts)];
        foreach ($availableLegalTexts as $documentId => $legalTextName) {
            array_push($contents, $legalTexts[$documentId]);
        }
        $response = $this->buildResponse(ApiContent::HTTP_OK, '', $contents[$index]);
        return function () use ($response) {
            return $response;
        };
    }

    /**
     * @param array $responseCodes
     * @param string $lang
     *
     * @return array
     */
    private function getExpectedDocuments($responseCodes, $lang)
    {
        $documents = [];
        $index = 0;
        $callback = $this->getResponseCallback($index, $lang);
        $response = $callback();
        $legalTexts = json_decode($response['content']);
        foreach ($legalTexts as $id => $name) {
            $responseCode = isset($responseCodes[$index]) ? $responseCodes[$index] : 200;
            if ($responseCode !== 200) {
                $index++;
                continue;
            }
            $responseCallback = $this->getResponseCallback($index + 1, $lang);
            $result = $responseCallback();
            $document = new Document();
            $document->id = $id;
            $document->name = $name;
            $document->text = $result['content'];
            $documents[$id] = $document;
            $index++;
        }

        return $documents;
    }

    private function buildResponse($statusCode, $messageKey, $content = '')
    {
        return compact("messageKey", "content", "statusCode");
    }
}
