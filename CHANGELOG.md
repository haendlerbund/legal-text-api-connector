# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.0] - 2022-06-06

### Added

- Full support for PHP 8

## [1.1.0] - 2021-12-17

### Added

- Support up to PHP 8.1

## [1.0.0] - 2020-10-09

### Added

- The format of the access token is validated
- The available languages can be requested
- The list of available legal texts can be requested in the available languages
- All legal texts for a shop can be requested
- The default API endpoint can be changed
- The default API key can be changed
- The Timeout for the response can be set
